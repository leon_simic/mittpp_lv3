import static io.restassured.RestAssured.delete;
import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasItems;

import io.restassured.parsing.Parser;

import io.restassured.RestAssured;
import org.hamcrest.Matchers;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
public class RESTAssuredJSONTests {
    final static String ROOT_URI = "http://localhost:7000/employees";

    static {
        // Registracija parsera za 'text/plain' kao JSON
        RestAssured.defaultParser = Parser.JSON;
    }

    @Test
    public void simple_get_test() {
        Response response = get(ROOT_URI);
        System.out.println(response.asString());
        response.then().body("id", hasItems("2", "3")); //
        response.then().body("name", hasItems("David"));
    }

    @Test
    public void post_test() {
        Response response = given().
                contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body("{\"id\": 4,\"name\": \"Lisa\",\"salary\": \"2000\"}")
                .when()
                .post(ROOT_URI);
        System.out.println("POST Response\n" + response.asString());
// tests
        response.then().body("id", Matchers.any(Integer.class));
        response.then().body("name", Matchers.is("Lisa"));
    }

    @Test
    public void put_test() {
        Response response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body("{\"name\": \"Lisa Tamaki\",\"salary\": \"45000\"}")
                .when()
                .put(ROOT_URI + "/3");
        System.out.println("PUT Response\n" + response.asString());
        response.then().body("name", Matchers.is("Lisa Tamaki"));
        response.then().body("salary", Matchers.is("45000"));
        response.then().body("id", Matchers.is("3"));
    }

    @Test
    public void delete_test() {
        Response response = delete(ROOT_URI + "/1");
        System.out.println(response.asString());
        System.out.println(response.getStatusCode());
// check if id=3 is deleted
        response = get(ROOT_URI);
        System.out.println(response.asString());
        response.then().body("id", Matchers.not(1));
    }

    @Test(dataProvider = "dpGetWithParam")
    public void get_with_param(int id, String name) {
        given()
                .pathParam("id", id)
                .when()
                .get(ROOT_URI + "/{id}", id)
                .then()
                .body("name", Matchers.is(name));
    }

    @DataProvider
    public Object[][] dpGetWithParam() {
        return new Object[][] {
                {3, "Lisa Tamaki"},
                {2, "David"}
        };
    }
}
